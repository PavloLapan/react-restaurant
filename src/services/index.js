import {Component} from 'react'


export default class FaturesData extends Component {

    data = [
        {
            id: "1",
            name: "DRAGON",
            src: 'https://yaponahata.com/upload/iblock/a43/a430fffe179805a0585944a13b647aa5.jpg',
            desc: "Dragon rolls are usually unique to the chef, and many get creative with the appearance of the dragon roll—some making them look like dragons. So there is some variation as to the ingredients chosen by different chefs, but dragon rolls are usually filled with eel and cucumber and topped with thinly-sliced avocado, producing a scale effect."
        },
        {
            id: "2",
            name: "OUR BEST",
            src: 'https://yaponahata.com/upload/iblock/f00/f00a5b5878c49629b69c37c053db7762.jpg',
            desc: "Tempura is a Japanese style of deep frying that uses a light batter. Tempura rolls can be made in two ways.\n" +
                "\n" +
                "As shown in the photo above, one way to make this crunchy delight is to make the entire roll tempura. In the photo above, the chef created sashimi rolls, covered it in tempura batter, and deep-fried the whole thing."
        },
        {
            id: "3",
            name: "GREEN DRAGON",
            src: 'https://yaponahata.com/upload/iblock/d00/d00da4a39503fb6e92f9ab0ac34582bd.jpg',
            desc: "Green dragon another way to make this crunchy delight is to prepare the contents tempura. For those rolls, shrimp tempura or some other kind of vegetable tempura is put inside the nori (seaweed paper)."

        },
        {
            id: "4",
            name: "NOMINAKI",
            src: 'https://yaponahata.com/upload/iblock/b6f/b6f033c788d1e446918f8426a0dc9772.jpg',
            desc: "Nominaki is a saltwater eel. Sushi usually uses a grilled slab of unagi coated or marinated in either oyster sauce, teriyaki sauce, or some other sweet-and-salty glaze. Unagi tastes like tender steak."
        },
        {
            id: "5",
            name: "CHEESE ELL ROLL",
            src: 'https://yaponahata.com/upload/iblock/b07/b0739367c2da1bcfbb7a8a3e486b151e.jpg',
            desc: "A ELL ROLL roll is a sushi roll topped with many different types of sashimi.\n" +
                "\n" +
                "The sushi roll underneath the sashimi is usually a California roll (avocado and crab).\n" +
                "\n" +
                "To make this type of sushi, the chef prepares a California roll and adds the toppings afterwards."
        }
        ,
        {
            id: "6",
            name: "Eel Avocado",
            src: 'https://yaponahata.com/upload/iblock/f01/f01934a346224e97b57fc289d0e81893.jpg',
            desc: "The Philly roll is a popular kind of sushi that you will find on many menus around the country. It usually has salmon, cream cheese, and cucumber, though it might come with other ingredients like avocado, onion, and sesame seed. It's named the Philly roll because of Philadelphia Cream Cheese, not because it's from Philadelphia."
        }
        ,
        {
            id: "7",
            name: "Philadelphia Eel Roll",
            src: 'https://yaponahata.com/upload/iblock/baa/baaa231e07eae92e973a0a48d156d7e3.jpg',
            desc: "A Philadelphia roll is usually made with crab and avocado. If you purchase a Philadelphia roll in a supermarket, you may get one with mayonnaise in it. In the Philadelphia roll above, there is crab, ahi (tuna), and avocado. Sometimes it will be served with a slab of ahi on top."
        }
    ];

    getData() {
        return new Promise((resolve, reject) => {
            setTimeout(() => {
                    resolve(this.data)
            }, 2000)
        })
    };
};