export const FETCH_MENU_CATEGORIES_BEGIN = "FETCH_MENU_CATEGORIES_BEGIN";
export const FETCH_MENU_CATEGORIES_SUCCESS = "FETCH_MENU_CATEGORIES_SUCCESS";
export const FETCH_MENU_CATEGORIES_FAILURE = "FETCH_MENU_CATEGORIES_FAILURE";

export const fetchMenuCategoriesBegin = () => ({
  type: FETCH_MENU_CATEGORIES_BEGIN
});

export const fetchMenuCategoriesSuccess = menuCategories => ({
  type: FETCH_MENU_CATEGORIES_SUCCESS,
  payload: { menuCategories }
});

export const fetchMenuCategoriesFailure = error => ({
  type: FETCH_MENU_CATEGORIES_FAILURE,
  payload: { error }
});

export function fetchMenuCategories() {
  return dispatch => {
    dispatch(fetchMenuCategoriesBegin());
    return fetch("https://api.oxygen-restourant.xyz/menuCategories")
      .then(handleErrors)
      .then(res => res.json())
      .then(json => {
        dispatch(fetchMenuCategoriesSuccess(json));
        return json.menuCategories;
      })
      .catch(error => dispatch(fetchMenuCategoriesFailure(error)));
  };
}

// Handle HTTP errors since fetch won't.
function handleErrors(response) {
  if (!response.ok) {
    throw Error(response.statusText);
  }
  return response;
}
