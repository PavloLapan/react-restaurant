export const FETCH_MENU_ITEMS_BEGIN = "FETCH_MENU_ITEMS_BEGIN";
export const FETCH_MENU_ITEMS_SUCCESS = "FETCH_MENU_ITEMS_SUCCESS";
export const FETCH_MENU_ITEMS_FAILURE = "FETCH_MENU_ITEMS_FAILURE";
export const FILTER_MENU_ITEMS = "FILTER_MENU_ITEMS";

export const fetchMenuItemsBegin = () => ({
  type: FETCH_MENU_ITEMS_BEGIN
});

export const fetchMenuItemsSuccess = menuItems => ({
  type: FETCH_MENU_ITEMS_SUCCESS,
  payload: { menuItems }
});

export const fetchMenuItemsFailure = error => ({
  type: FETCH_MENU_ITEMS_FAILURE,
  payload: { error }
});

export const filterMenuItems = payload => ({
  type: FILTER_MENU_ITEMS,
  payload: payload
});

export function fetchMenuItems() {
  return dispatch => {
    dispatch(fetchMenuItemsBegin());
    return fetch("https://api.oxygen-restourant.xyz/menuItems")
      .then(handleErrors)
      .then(res => res.json())
      .then(json => {
        dispatch(fetchMenuItemsSuccess(json));
        return json.menuItems;
      })
      .catch(error => dispatch(fetchMenuItemsFailure(error)));
  };
}

// Handle HTTP errors since fetch won't.
function handleErrors(response) {
  if (!response.ok) {
    throw Error(response.statusText);
  }
  return response;
}
