import { combineReducers } from "redux";
import menuCategories from "./menuCategoryReducer";
import menuItems from "./menuItemReducer";
import features from "./featureReducer";

export default combineReducers({
  menuCategories,
  menuItems,
  features
});
