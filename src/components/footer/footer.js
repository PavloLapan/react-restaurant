import React from 'react';
import thumb8 from '../../img/thumb8.png'
import thumb9 from '../../img/thumb9.png'
import RecentPost from './recent_post'

const Footer = () => {
    return(
        <footer>
            <div className="container">
                <div className="row justify footer">
                    <div className="w-25">
                        <h2>ABOUT US</h2>
                        <p>Our dishes are served independently from rice and noodles which are ordered separately to give variety.  Your chosen dishes can be complemented by vegetables or noodles to accompany your meal.  Individual dishes can be shared by all in this way to allow everyone to enjoy the different tastes of the Orient.</p>
                        <small>Read more →</small>
                    </div>
                    <div className="w-25">
                        <h2>RECENT POST</h2>
                            <RecentPost
                                postImg={thumb8}
                                postDate="29 may 2019"
                                postText="Hand picked ingredients for our best customers"
                            />
                        <div className="mt-1">
                            <RecentPost
                                postImg={thumb9}
                                postDate="30 may 2019"
                                postText="Daily special foods that you will going to love"/>
                        </div>
                    </div>
                    <div className="w-25">
                        <h2>REACH US</h2>
                        <div className="row social ">
                            <a href="https://www.facebook.com/"><i className="fa fa-facebook-square" aria-hidden="true"></i></a>
                            <a href="https://vk.com/"><i className="fa fa-vk" aria-hidden="true"></i>
                            </a>
                            <a href="https://www.whatsapp.com/?lang=uk"><i className="fa fa-whatsapp" aria-hidden="true"></i>
                            </a>
                            <a href="https://web.telegram.org/#/login"><i className="fa fa-telegram" aria-hidden="true"></i>
                            </a>
                            <a href="https://www.instagram.com/?hl=ru"><i className="fa fa-instagram" aria-hidden="true"></i>
                            </a>
                        </div>
                        <div className=" contact-us column">
                            <p><i className="fa fa-map-marker"></i>28 Seventh Avenue,New York, 10014</p>
                            <p><i className="fa fa-phone"></i>Phone: (415) 124-5678</p>
                            <p><i className="fa fa-envelope-o"></i>support@restaurant.com</p>
                        </div>
                    </div>
                </div>
            </div>
            <div className="copyright">
                <p><i className="fa fa-copyright"></i>2019. Oxygen. All rights reserved. Designed with <i
                    className="fa fa-heart primary-color"></i> by Pavlo Lapan</p>
            </div>
        </footer>
    )
}

export default Footer