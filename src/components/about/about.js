import React from 'react';
import '../../style/style.css'
import '../../style/media.css'

import image1 from '../../img/thumb1.png'
import image2 from '../../img/thumb2.png'
import image3 from '../../img/thumb3.png'
import image4 from '../../img/signature.png'
import '../../style/carousel.css'

 const About = () => { 
    return(
<div>
    <section className="about" id = "about">
        <div className="row column text-center">
            <h2>
                THE RESTAURANT
            </h2>
            <p className="blue-line">
                A little about us and a breif history of how we started.
            </p>
        </div>
        <div className="row media">
            <div className="col-4">
                <img src={image1} alt=""/>
                <div className="images d-flex ">
                    <img src={image2} alt=""/>
                    <img src={image3} alt=""/>
                </div>
            </div>
            <div className="col-6">
                <p>The untreated tables of green oak that twist and buckle around black granite panels, lace the restaurant with a quirky and unique sense of life.</p>
                <p>Squared against the clean lines of polished glassware and cleverly designed tableware, the food is designed by Nick Gillespie, with over 20 years’ experience behind the stove looks to cherry pick his favourite methods, techniques and influences from around the world.</p>
                <img src={image4} alt=""/>
            </div>
        </div>
    </section>
    <section className="actions">
        <div className="container">
            <div className="row text-center column">
                <h2 className="white">
                    TODAY'S SPECIALS
                </h2>
                <p className="blue-line">
                    A little about us and a breif history of how we started.
                </p>
            </div>
            <div className="row">
                <div className="carousel-wrapper">
                    <span id="target-item-1"></span>
                    <span id="target-item-2"></span>
                    <span id="target-item-3"></span>
                    <div className="carousel-item item-1 light">
                        <h2>KAFUKU CHEESE (24 PEACES)</h2>
                        <p>Pieces of raw fish placed on top of a bowl of rice; recommended for those wanting to try a variety of fish or a lot of one kind at a cheap price. Literally “scattered sushi”
                        </p>
                        <a className="arrow arrow-prev" href="#target-item-3"/>
                        <a className="arrow arrow-next" href="#target-item-2"/>
                    </div>
                    <div className="carousel-item item-2 light">
                        <h2>SET VEGA (22 PEACES)</h2>
                        <p>The most common type of sushi; involves a slice of raw fish or other topping on top of an oblong mound of rice. Also known as Edo-mae sushi
                        </p>
                        <a className="arrow arrow-prev" href="#target-item-1"></a>
                        <a className="arrow arrow-next" href="#target-item-3"></a>
                    </div>
                    <div className="carousel-item item-3 light">
                        <h2>HONABI (44 PEACES)</h2>
                        <p>A California roll or California maki is a makizushi sushi roll that is usually rolled inside-out, and containing cucumber, crab or imitation crab, and avocado.
    </p>
                        <a className="arrow arrow-prev" href="#target-item-2"></a>
                        <a className="arrow arrow-next" href="#target-item-1"></a>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>)
};

export default About