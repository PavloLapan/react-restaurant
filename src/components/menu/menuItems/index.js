import React from "react";
import { connect } from "react-redux";
import MenuItem from "./menu-list-item";
import Spinner from "../../spinner/spinner";
import ErrorIndicator from "../../error-indicator/error-indicator";
import { fetchMenuItems } from "../../../actions/menuItemActions";

class MenuItemList extends React.Component {
  componentDidMount() {
    this.props.dispatch(fetchMenuItems());
  }

  render() {
    const { error, loading, menuItems, filteredItems } = this.props;

    if (loading) {
      return <Spinner />;
    } else if (error) {
      return <ErrorIndicator />;
    } else {
      return (
        <div className="d-flex">
          <div className="col-12">
            {(filteredItems || menuItems).map((item, index) => {
              return <MenuItem key={index} item={item} />;
            })}
          </div>
        </div>
      );
    }
  }
}

const mapStateToProps = ({
  menuItems: { filteredItems, items, loading, error }
}) => ({
  filteredItems: filteredItems,
  menuItems: items,
  loading: loading,
  error: error
});

export default connect(
  mapStateToProps,
  null
)(MenuItemList);
