import React from "react";
import styles from "./style.module.css";
import { connect } from "react-redux";
import { filterMenuItems } from "../../../actions/menuItemActions";

class MenuCategory extends React.Component {
  render() {
    const { category, menuItems } = this.props;
    return (
      <p
        className={styles.p}
        onClick={e => this.props.handleClick(category, menuItems)}
      >
        {category.name}
      </p>
    );
  }
}

const mapStateToProps = ({ menuItems: { items } }) => ({
  menuItems: items
});

const mapDispatchToProps = dispatch => ({
  handleClick: (category, menuItems) => {
    dispatch(
      filterMenuItems(
        category.name === "ALL"
          ? menuItems
          : menuItems.filter(item => item.menu_category_id === category.id)
      )
    );
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(MenuCategory);
