import React from "react";
import { connect } from "react-redux";
import MenuCategory from "./index";
import Spinner from "../../spinner/spinner";
import ErrorIndicator from "../../error-indicator/error-indicator";
import { fetchMenuCategories } from "../../../actions/menuCategoryActions";

class MenuCategoryList extends React.Component {
  componentDidMount() {
    this.props.dispatch(fetchMenuCategories());
  }

  render() {
    const { error, loading, menuCategories } = this.props;

    if (loading) {
      return <Spinner />;
    } else if (error) {
      return <ErrorIndicator />;
    } else {
      return (
        <div className="row list">
          <MenuCategory key="0" category={{ name: "ALL" }} />
          {menuCategories.map((item, index) => {
            return <MenuCategory key={index + 1} category={item} />;
          })}
        </div>
      );
    }
  }
}

const mapStateToProps = ({ menuCategories: { items, loading, error } }) => ({
  menuCategories: items,
  loading: loading,
  error: error
});

export default connect(
  mapStateToProps,
  null
)(MenuCategoryList);
